import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostBinding,
} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-reservas',
  templateUrl: './lista-reservas.component.html',
  styleUrls: ['./lista-reservas.component.scss'],
})
export class ListaReservasComponent implements OnInit {
  @Input() reservados: DestinoViaje[];
  @Output() anulado = new EventEmitter();
  @HostBinding('attr.class') cssClasses: string =
    'container content d-flex flex-row flex-wrap justify-content-center';

  constructor() {}

  ngOnInit(): void {}

  onAnular(e) {
    this.anulado.emit(e);
  }
}
