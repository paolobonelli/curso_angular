export class DestinoViaje {
  name: string;
  url: string;
  reservado: boolean;
  boletos: number;

  constructor(n: string, u: string, r: boolean = false, b: number = 1) {
    this.name = n;
    this.url = u;
    this.reservado = r;
    this.boletos = b;
  }
}
