import { Component, Query } from '@angular/core';
import { DestinoViaje } from './models/destino-viaje.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  sinReserva: DestinoViaje[];
  reservados: DestinoViaje[];

  title = 'angular-wishlist';

  constructor() {
    this.sinReserva = [
      new DestinoViaje('la cocina', '../../assets/media/cocina.jpeg'),
      new DestinoViaje('La sala', '../../assets/media/sala.jpg'),
      new DestinoViaje('El Comedor', '../../assets/media/comedor.jpg'),
      new DestinoViaje('El Balcón', '../../assets/media/balcon.jpg'),
    ];
    this.reservados = [];
  }

  funReservar(e) {
    e.reserva.reservado = true;
    this.reservados = this.reservados.concat(e.reserva);
    this.sinReserva = this.sinReserva.filter((dest) => {
      return dest != e.reserva;
    });
  }
  funAnular(e) {
    e.reserva.reservado = false;
    e.reserva.boletos = 1;
    this.sinReserva = this.sinReserva.concat(e.reserva);
    this.reservados = this.reservados.filter((dest) => {
      return dest != e.reserva;
    });
  }
  reservar(aReservar, boletos, e) {
    boletos = Number(boletos);
    aReservar = this.sinReserva.find((dest) => {
      return dest.name == aReservar;
    });
    if (boletos > 1) {
      aReservar.boletos = boletos;
    }
    this.funReservar({ event: e, reserva: aReservar });
  }
}
