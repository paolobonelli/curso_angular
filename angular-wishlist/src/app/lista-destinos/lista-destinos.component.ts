import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  HostBinding,
} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss'],
})
export class ListaDestinosComponent implements OnInit {
  @Input() destinos: DestinoViaje[];
  @Output() reserva = new EventEmitter();
  @HostBinding('attr.class') cssClasses: string =
    'container content d-flex flex-row flex-wrap justify-content-center';

  constructor() {}

  ngOnInit(): void {}

  onReservar(e) {
    this.reserva.emit(e);
  }
}
