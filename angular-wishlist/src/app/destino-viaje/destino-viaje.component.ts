import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss'],
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Output() reserva = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  mover(e) {
    this.reserva.emit({ event: e, reserva: this.destino });
  }
}
