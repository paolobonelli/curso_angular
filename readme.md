# Mi Angular-Wishlist

[ENG] This repository contains an Angular SPA to reserve your travel into your home

[ESP] Este repositorio contiene una SPA de Angular para reservar tu viaje dentro de tu casa

> ## Important notes // Notas importantes:
>
> > [ENG] Please install Node JS, typescript and Angular-cli
> >
> > [ESP] Por favor instala Node JS, typescript y Angular-cli

> ## Usage [ENG]
>
> - Clon the repository with `git clone https://bitbucket.org/paolobonelli/curso_angular.git`
>
> - Go to the project's directory with `cd ./angular-wishlist/`
>
> - Install the dependencies with `npm install --save`
>
> > - Go to branch of evaluation:
> >
> > > - Week One (01): `git checkout semana1`
> >
> > - Start Server with `ng serve`

> ## Uso [ESP]
>
> - Clona el repositorio con `git clone https://bitbucket.org/paolobonelli/curso_angular.git`
>
> - Ve al directorio del proyecto con `cd ./angular-wishlist/`
>
> - Instala las dependencias con `npm install --save`
>
> - Ve a la rama (branch) de la semana deseada con:
>
> > - Ir a las ramas de evaluación:
> >
> > > - Semana Uno (01): `git checkout semana1`
> >
> > - Corre el servidor con `ng serve`
>
> - Enjoy!
